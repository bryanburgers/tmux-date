use chrono::{DateTime, Local, TimeZone, Utc};
use chrono_tz::Tz;

fn main() {
    let local = get_localtime();
    let utc = Utc::now();

    let local_date = local.format("%Y-%m-%d");
    let local_time = local.format("%H:%M");
    let local_offset = local.format("%:z").to_string();
    let local_tzname = local.format("%Z").to_string();
    let local_offset = if local_offset == local_tzname {
        local_offset
    } else {
        format!("{} ({})", local_tzname, local_offset)
    };
    let utc_date = utc.format("%Y-%m-%d");
    let utc_time = utc.format("%H:%M");

    // If the date in the UTC timezone is the same as it is in the current timezone, then
    // displaying the UTC date is just adding time. However, if the dates differ, then display the
    // date. Having the date displayed will call attention to itself that the UTC date and the
    // local date are currently different.
    if format!("{}", local_date) == format!("{}", utc_date) {
        println!(
            "{} {} {} / {}Z",
            local_date, local_time, local_offset, utc_time
        );
    } else {
        println!(
            "{} {} {} / {}T{}Z",
            local_date, local_time, local_offset, utc_date, utc_time
        );
    }
}

// I'm not sure how I can return _either_ a `DateTime<chrono_tz::Tz>` or a `DateTime<Local>` from a
// function. So create this trait.
trait DateTimeWithDynamicTimeZone {
    fn format<'a>(
        &self,
        fmt: &'a str,
    ) -> chrono::format::DelayedFormat<chrono::format::StrftimeItems<'a>>;
}

impl<Tz> DateTimeWithDynamicTimeZone for DateTime<Tz>
where
    Tz: TimeZone,
    Tz::Offset: std::fmt::Display,
{
    fn format<'a>(
        &self,
        fmt: &'a str,
    ) -> chrono::format::DelayedFormat<chrono::format::StrftimeItems<'a>> {
        self.format(fmt)
    }
}

#[cfg(target_os = "macos")]
mod macos;

#[cfg(target_os = "macos")]
fn get_localtime() -> Box<dyn DateTimeWithDynamicTimeZone> {
    let timezone_name = macos::get_timezone_name();

    let b: Box<dyn DateTimeWithDynamicTimeZone> = match timezone_name.parse::<Tz>() {
        Ok(tz) => Box::new(Utc::now().with_timezone(&tz)),
        Err(_) => Box::new(Local::now()),
    };

    b
}

#[cfg(not(target_os = "macos"))]
fn get_localtime() -> Box<dyn DateTimeWithDynamicTimeZone> {
    Box::new(Local::now())
}
