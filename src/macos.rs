use core_foundation::timezone::CFTimeZone;

pub fn get_timezone_name() -> String {
    let timezone = CFTimeZone::system();
    timezone.name().to_string()
}
