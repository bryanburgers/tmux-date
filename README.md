# tmux-date

Format the current date in a way that clearly shows local time and UTC time,
for use in my tmux status bar.


## Examples

```
tmux-date  (with computer set to America/Chicago timezone)
   2016-01-11 08:55 CST (-06:00) / 14:55Z
TZ="Australia/Victoria" tmux-date
   2016-01-12 01:55 AEDT +11:00 / 2016-01-11T14:55Z
```


## Why not create a script to do this?

I did. Unfortunately, the implementation of `date` differs significantly from
Linux to MacOS, to the extent that one or the other failed. Specifically, Linux
has formatting strings that MacOS does not.


## Why Rust?

Because I'm enjoying learning rust, and because I can easily create binaries
that work in Linux and MacOS that require no other dependencies.


## Compiling for Linux from MacOS

```
docker run -it --rm -v $PWD:/src -w /src rust:latest sh -c 'cargo build --release --target x86_64-unknown-linux-gnu && strip target/x86_64-unknown-linux-gnu/release/tmux-date'
```
